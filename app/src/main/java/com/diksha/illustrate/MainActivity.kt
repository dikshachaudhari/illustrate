package com.diksha.illustrate

import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import top.defaults.colorpicker.ColorPickerPopup
import top.defaults.colorpicker.ColorPickerPopup.ColorPickerObserver


class MainActivity : AppCompatActivity() {
    private lateinit var drawingView: drawingBoardView

    private var brushSizeCode: Int = 0
    private var selectedBrushSize = 5.0F


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        drawingView = findViewById<drawingBoardView>(R.id.drawingView)

        drawingView.setBrushSize(5.0F)
    }

    public fun onShowBrushDialog(view: View){
        val brushDialog = Dialog(this)
        brushDialog.setContentView(R.layout.dialog_brush)
        brushDialog.setTitle("Select Brush Size")

        brushDialog.show()

        val smallBrushBtn = brushDialog.findViewById<ImageButton>(R.id.btnSmallBrush)
        val mediumBrushBtn = brushDialog.findViewById<ImageButton>(R.id.btnMediumBrush)
        val largeBrushBtn = brushDialog.findViewById<ImageButton>(R.id.btnLargeBrush)

        changeBrushSizeBorder(brushSizeCode, smallBrushBtn, mediumBrushBtn, largeBrushBtn)

        smallBrushBtn.setOnClickListener {
            drawingView.setBrushSize(4.0F)
            brushSizeCode = 1

            brushDialog.dismiss()
        }

        mediumBrushBtn.setOnClickListener {
            drawingView.setBrushSize(8.0F)
            brushSizeCode = 2

            brushDialog.dismiss()
        }

        largeBrushBtn.setOnClickListener {
            drawingView.setBrushSize(12.0F)
            brushSizeCode = 3

            brushDialog.dismiss()
        }

    }

    public fun onShowColorPicker(view: View) {
        ColorPickerPopup.Builder(this)
                .initialColor(Color.RED) // Set initial color
                .enableBrightness(true) // Enable brightness slider or not
                .enableAlpha(true) // Enable alpha slider or not
                .okTitle("Choose")
                .cancelTitle("Cancel")
                .showIndicator(true)
                .showValue(true)
                .build()
                .show(view, object : ColorPickerObserver() {
                    override fun onColorPicked(color: Int) {
                        drawingView.setColor(color)
                    }

                    fun onColor(color: Int, fromUser: Boolean) {}
                })
    }


    private fun changeBrushSizeBorder(brushSizeButtons: Int, smallBrushBtn: ImageButton, mediumBrushBtn: ImageButton, largeBrushBtn: ImageButton){
        smallBrushBtn.setBackgroundResource(R.drawable.default_brush_border)
        mediumBrushBtn.setBackgroundResource(R.drawable.default_brush_border)
        largeBrushBtn.setBackgroundResource(R.drawable.default_brush_border)
        when(brushSizeButtons) {
            1 -> smallBrushBtn.setBackgroundResource(R.drawable.selected_brush_border)
            2 -> mediumBrushBtn.setBackgroundResource(R.drawable.selected_brush_border)
            3 -> largeBrushBtn.setBackgroundResource(R.drawable.selected_brush_border)
        }
    }
}