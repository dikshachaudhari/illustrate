package com.diksha.illustrate

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.SizeF
import android.util.TypedValue
import android.view.MotionEvent
import android.view.View

class drawingBoardView(context: Context, attrs: AttributeSet) : View(context, attrs) {

    private var drawingPath: CustomPath? = null   //this drawingPath is object of CustomPath
    private var canvasBitmap: Bitmap? = null      //canvas ka object initialise karne ke liye we need a bitmap ka object
    private var drawingPaint: Paint? = null
    private var canvasPaint: Paint? = null
    private var canvas: Canvas? = null
    private var brushSize: Float = 0.0F           //default
    private var color = Color.BLACK               //default

    private var pathList = ArrayList<CustomPath>()   //array list ka object

    init {                                        //constructor
        setUpDrawing()
    }

    private fun setUpDrawing() {                  //to initialise everything
        drawingPaint = Paint()

        drawingPaint!!.color
        drawingPaint!!.style = Paint.Style.STROKE
        drawingPaint!!.strokeJoin = Paint.Join.ROUND
        drawingPaint!!.strokeCap = Paint.Cap.ROUND

        brushSize = 15.0F

        canvasPaint = Paint(Paint.DITHER_FLAG)
        drawingPath = CustomPath(color, brushSize)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {      //this method will only be called once (one time event method)
        super.onSizeChanged(w, h, oldw, oldh)

        canvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)

        canvas = Canvas(canvasBitmap!!)
    }

    // called when the view renders the content, so it would be called for every pixel
    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        canvas?.drawBitmap(canvasBitmap!!, 0F, 0F, canvasPaint)

        if(! drawingPath!!.isEmpty) {
            drawingPaint!!.strokeWidth = drawingPath!!.brushThickness
            drawingPaint!!.color = drawingPath!!.color
            canvas?.drawPath(drawingPath!!, drawingPaint!!)
        }

        for(path in pathList) {
            drawingPaint!!.strokeWidth = path!!.brushThickness
            drawingPaint!!.color = path!!.color
            canvas?.drawPath(path!!, drawingPaint!!)
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        val touchX = event?.x
        val touchY = event?.y

        when(event?.action) {
            MotionEvent.ACTION_DOWN -> {
                drawingPath!!.color = color
                drawingPath!!.brushThickness = brushSize

                drawingPath!!.reset() //safer side call so that nothing of the previous path is included(last path ko null karega)

                drawingPath!!.moveTo(touchX!!, touchY!!)
                /*if(touchX != null && touchY != null) {
                    drawingPath!!.moveTo(touchX, touchY)
                }*/
            }

            MotionEvent.ACTION_MOVE -> {
                if(touchX != null && touchY != null) {
                    drawingPath!!.lineTo(touchX, touchY)
                }
            }

            MotionEvent.ACTION_UP -> {
                pathList.add(drawingPath!!);
                drawingPath = CustomPath(color, brushSize)      // naya color and brushsize
            }

            else -> return false
        }

        invalidate()
        return true    //event successfully processed

        return super.onTouchEvent(event)
    }

    fun setBrushSize(newBrushSize: Float) {
        brushSize = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                newBrushSize,
                resources.displayMetrics
        )
        drawingPaint!!.strokeWidth = brushSize
    }

    fun setColor(newColor: Int){
        color = newColor
        drawingPaint!!.color = newColor
    }

    internal inner class CustomPath(var color: Int, var brushThickness: Float): Path(){   /*this has to inherit Path*/

    }

}